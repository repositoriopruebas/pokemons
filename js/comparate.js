var minWeight = 10000;
var minExperience = 10000;
var maxWeight = 0;
var limit = 1050;
var search_url = 'https://pokeapi.co/api/v2/pokemon?offset=0&limit='+limit+'';
var pokemon;
var pokemonheavier;
var pokemonexperiencie;
var pokemonlessexperiencie;
var heaviersOnOff = false;
var maxExperiencie = 0;
var theHeaviers = [];
var theLighters = [];
var theTallers = [];
var theSmallers = [];
var theMoreExperiencie = [];
var theLessExperiencie = [];
const moreExperiencie = "More Experiencie Pokemons";
const lessExperiencie = "Less Experiencie Pokemons";
const lighters = "The lighters Pokemons";
const heaviers = "The heaviers Pokemons";
//https://d2skuhm0vrry40.cloudfront.net/2019/articles/2019-11-15-18-17/Pokemon_Sword_Starter_Pokemon.jpg
//https://pokeapi.co/api/v2/pokemon?offset=20&limit=20
searchPokemons();
async function searchPokemons()
{
	try
	{
		let res = await fetch(search_url);
		var responsepokemon = await res.json();
	    transformUrl(responsepokemon);
	}catch(err)
    {
	      console.log(err);	    
    }  	
}
function transformUrl(poke)
{
	var pokemonarray = []
	for(let i=0;i<poke.results.length;i++)
	{
		pokemonarray.push(poke.results[i].url);
	}
	preparePokemons(pokemonarray);
}
async function preparePokemons(poke)
{	
	pokemonArray = [];
	var percent;
	try
	{	
		for(let i=0; i<poke.length;i++)
		{
			let res = await fetch(poke[i]);
			var responsepokemon = await res.json();
			pokemonArray.push(responsepokemon);	
			percent =	(i / poke.length) * 100;
			progressBar(percent)	
		}
		
		compareLightest(pokemonArray);
		compareHeaviest(pokemonArray);
		compareMoreExperiencie(pokemonArray);
		compareLessExperiencie(pokemonArray);
		document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(lighters)	
	}catch(err)
	{
		console.log(err.status);
	}	
}
function progressBar(percent)
{
	var percentInteger = Math.round(percent);
	console.log(Math.round(percent))
	$('#progreso').css('width',percentInteger+"%");
	$('#progreso').text(percentInteger+"%");
}
function jumbotronComponent(size)
{
	var jumboComponent = '<div  class="jumbo-nav">'+
								'<nav style="margin-bottom:-8%" class="navbar navbar-expand-md navbar-light">'+

								    '<a href="#" class="navbar-brand"></a>'+
								    '<button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">'+
								        '<span class="navbar-toggler-icon"></span>'+
								    '</button>'+
								    '<div class="collapse navbar-collapse bg-info " id="navbarCollapse">'+
								        '<div class="navbar-nav nav-info">'+
								            '<a href="#" style="color:white" class="nav-item nav-link heaviers menu">'+heaviers+'</a>'+
								            '<a href="#" style="color:white" class="nav-item nav-link lighters menu active">'+lighters+'</a>'+
								            '<a href="#" style="color:white" class="nav-item nav-link moreExperiencie menu">'+moreExperiencie+'</a>'+
								            '<a href="#" style="color:white" class="nav-item nav-link lessExperiencie menu" tabindex="-1">'+lessExperiencie+'</a>'+
								        '</div>'+
								    '</div>'+
								'</nav>'+
                         '</div>'
	return jumboComponent;                         
}
function compareHeaviest(poke)
{	var cont = 0;
	pokemonheavier;
	for(let i = 0; i<poke.length; i++)
	{	
		if(poke[i].weight > maxWeight)
		{
			maxWeight = poke[i].weight;
			if(cont > 4)
			{
				pokemonheavier = poke[i];
				theHeaviers.push(poke[i]);
			}
			cont++;
		}		
	}
}
function compareMoreExperiencie(pokeex)
{	var cont = 0;
	pokemonexperiencie;
	for(let i = 0; i<pokeex.length; i++)
	{	
		if(pokeex[i].base_experience > maxExperiencie)
		{
			maxExperiencie = pokeex[i].base_experience;
			if(cont > 2)
			{
				pokemonexperiencie = pokeex[i];
				theMoreExperiencie.push(pokeex[i]);
			}
			cont++;
		}		
	}
}
function compareLessExperiencie(poke)
{
	var cont = 0;
	pokemonlessexperiencie;
	for(let i = 0; i<poke.length; i++)
	{	
		if(poke[i].base_experience < minExperience && poke[i].base_experience != null)
		{
			minExperience = poke[i].base_experience;
			pokemonlessexperiencie = poke[i];
			theLessExperiencie.push(poke[i]);			
			cont++;
		}		
	}
}
function seeLessExperience()
{
	document.querySelector('.jumbotron .row').innerHTML = '';	
	for(let i = 0; i<theLessExperiencie.length; i++)
	{
		compareRender(theLessExperiencie[i]);
	}
}
function seeExperiencie()
{
	console.log(theMoreExperiencie);
	document.querySelector('.jumbotron .row').innerHTML = '';	
	for(let i = 0; i<theMoreExperiencie.length; i++)
	{
		compareRender(theMoreExperiencie[i]);
	}
}

function seeHeaviers()
{
	console.log(theHeaviers);
	document.querySelector('.jumbotron .row').innerHTML = '';	
	for(let i = 0; i<theHeaviers.length; i++)
	{
		compareRender(theHeaviers[i]);
	}
}
function compareLightest(poke)
{	var cont = 0;
	var pokemonlight;
	for(let i=0; i<poke.length;i++)
	{		
		if(poke[i].weight <= minWeight)
		{
			minWeight = poke[i].weight;
			if(cont > 4)
			{
				pokemonlight = poke[i];	
				compareRender(pokemonlight);
				theLighters.push(poke[i]);
			}
			cont++;	
		}			
	}
}
function seeLighters()
{
	document.querySelector('.jumbotron .row').innerHTML = '';	
	for(let i = 0; i<theLighters.length; i++)
	{
		compareRender(theLighters[i]);
	}  
}
function pokemonComponent(pokemon)
{ 
    component ='<figure class="badge badge-secondary">'+                
	                    '<figcaption >'
	                          +pokemon.name+'<br>'+        
	                          '<input type="hidden" id="ocultovalor'+pokemon.id+'" value="'+pokemon.id+'">'+                             
	                          '<img  class="img-thumbnail bg-info " width="95px;"  onerror ="errores()" id="img'+pokemon.id+'" src="'+pokemon.sprites.front_default+'" alt="'+pokemon.name+'"><br>'+ 
	                          'Show More'+
	                    '</figcaption>'+
	            '</figure>'   
    return component;
}
function compareComponent(lighter)
{
	var compareComponent = '<div class="animate" >'+ 
								'<hr class="my-4">'+
  					  	   		lighter+
  					  	   '</div>'
  	return compareComponent;				  	   
}
function compareRender(pokemonlighter)
{
	var render = document.querySelector('.jumbotron .row');	
	var lighter = pokemonComponent(pokemonlighter);
	render.innerHTML+= compareComponent(lighter);	
}
function rotar()
{			
	var rotation = function (){
		   $("#pokebola").rotate({
		      angle:0, 
		      animateTo:360, 
		      callback: rotation
		   });
		}
	rotation();	
}

 