var url = '';
  var template = '';  
  var prevLink;
  var nextLink;
  var cont = 1;
  var interruptor = false;
  var timer = 2200;
  var imgs = '';
  var contclick = 1;
  var interruptortemplate = false;        
  const d = document,
  $numeropagina = d.querySelector('#numeropagina');
  $main = d.querySelector('main');
  $link = d.querySelector('.links')
  function init()
  {
  	url = "https://pokeapi.co/api/v2/pokemon";
  	$numeropagina.innerHTML = "Page: "+cont++;
  	configureAjaxCalls(url);
    rotar();
  }
  function configureAjaxCalls(url) {
  	 $main.innerHTML = 'loading....';
     fetch(url)
        .then(ajaxPositive)
        .catch(showError);              
    };            
  function ajaxPositive(response) {
      console.log('response.ok: ', response.ok);
      if(response.ok) {
        response.json().then(showResult);
      } else {
        showError('status code: ' + response.status);
      }
    }
  function showResult(txt) 
  {
      var move_sprites = '';
      var pokemon = '';
      for(let i=0; i<txt.results.length; i++)
      {
      	render(txt.results[i].url)
      }	
      //console.log('muestro respuesta: ', txt)
      buttonLinksControl(txt);      
  }
  function errores()
  {
      d.querySelector('img').setAttribute('src','assets/cargando-loading-035.gif')
  }
  async function render(url)
  {
      //console.log('muestro respuesta: ', txt.results[i]);
        try 
        {
          let res = await fetch(url);
          pokemon = await res.json();                      
          //console.log(Object.keys(pokemon.sprites));
          if(interruptor == true)
          {
            console.log(pokemon);            
            renderObservable();           
          }else
          {
            template += pokemonComponent(pokemon);

          }          
        }catch(err)
        {
          console.log(err);
          template += '<figure>'+
                'Error'+err.status+
                '</figure>'
        }       
        $main.innerHTML = template; 
        interruptor = false; 

  }
  function renderObservable()
  {           
       var sprites = Object.values(pokemon.sprites)
       var images;   
       const observablesprites =  Rx.Observable.from(sprites)
          .take(8)
          .filter(x => x != null)
         // .concatMap(value => Rx.Observable.timer(timer).map(_=> value))
        const sprites_subs = observablesprites.subscribe(d => {
               
              imgs += '<div class="img-thumbnail bg-info carousel-item">'+
                          '<img width="100%" src="'+d+'">'+
                      '</div>'      
          //document.querySelector('img').setAttribute('src',d);
          });
          $('#modalLabel').text(pokemon.name.toUpperCase());
          var ability;
          title = '<br><span style="color:white">Abilities</span>'
          $.each(pokemon.abilities, function(key,value){
                          
                ability += '<li id="abilities">'+value.ability.name+'</li>';
                console.log(ability);

                })
          $('#data-pokemon').html(              
              '<ul>'+
                
                '<li>Name : '+pokemon.name+'</li>'+
                '<li>Base Experiencie: '+pokemon.base_experience+'</li>'+
                '<li>Height: '+pokemon.height+'</li>'+
                '<li>Order: '+pokemon.order+'</li>'+
                '<li>Weight: '+pokemon.weight+'</li>'+
                title+
                ability+
                
              '</ul>'
            )
          $('#carouselControls').append(            
                    '<div class="carousel-inner">'+
                          '<div class="carousel-item active">'+
                              '<img class="img-thumbnail bg-info" width="100%" src="'+pokemon.sprites.front_default+'">'+
                          '</div>'+
                          imgs+
                    '</div>'+
                    '<a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">'+
                    '<span class="carousel-control-prev-icon" aria-hidden="true">'+
                    '</span>'+
                    '<span class="sr-only">Previous</span>'+    
                    '</a>'+
                    '<a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">'+
                        '<span class="carousel-control-next-icon" aria-hidden="true">'+
                        '</span>'+
                        '<span class="sr-only">Next</span>'+
                    '</a>'
            ); 
          imgs = '';  
          sprites_subs.dispose();    
  }
  function deletePictures()
  {
      $('.carousel-inner').empty(); 
  }
  function buttonLinksControl(txt)
  {     
      prevLink = txt.previous ? '<a class="btn btn-info" id="previous" href="'+txt.previous+'"><</a>':""      
  	  nextLink = txt.next ? '<a class="btn btn-info" id="next" href="'+txt.next+'">></a>':"";
  	  $link.innerHTML = prevLink + " "+nextLink;
  	  template = '';
  }
  function showError(err) 
  { 
      console.log('muestor error', err);
  }

  d.addEventListener('DOMContentLoaded', init);
	d.addEventListener('click',e => {
   
  if(e.target.matches('button') )
  {
     deletePictures();
  }
 	if(e.target.matches('.links a'))
 	{
 		e.preventDefault();
 		configureAjaxCalls(e.target.getAttribute('href')); 
 		if(e.target.id == 'next')
 		{
      contclick++; 		
      $numeropagina.innerHTML = "Page: "+contclick;	
 		}else
 		{
      contclick--;
 			$numeropagina.innerHTML = "Page: "+contclick;
 		}
    const pagObservable = Rx.Observable
                            .fromEvent(d.querySelector('.links'),'click')
                            .scan(x => x + contclick,1 )
                            .filter(x => x > 0 && x<170)
                            
    const subcsripPag = pagObservable.subscribe();
    subcsripPag.dispose();
 	}
  if(e.target.matches('figcaption'))
  {       
    var captions = e.target.querySelector('input');       
    var searchurl = url+"/"+captions.getAttribute('value');
    interruptor = true;
    cancel_subscription = false;
    timer = 2200;
    render(searchurl);
    $('#modal').modal({backdrop: 'static', keyboard: false});
  }
  if(e.target.matches('.jumbo-nav .heaviers') )
  {
    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(heaviers);
    seeHeaviers(); 
    d.querySelector('.lighters').classList.remove('active');  
    d.querySelector('.heaviers').setAttribute('class','nav-item nav-link lighters menu active');
  
  } 
  if(e.target.matches('.jumbo-nav .lighters') )
  {    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(lighters);    
    seeLighters(); 
    d.querySelector('.lighters').setAttribute('class','nav-item nav-link lighters menu active');
    
   } 
  if(e.target.matches('.jumbo-nav .moreExperiencie') )
  {    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(moreExperiencie); 
    d.querySelector('.lighters').classList.remove('active');    
    seeExperiencie();   
    d.querySelector('.moreExperiencie').setAttribute('class','nav-item nav-link lighters menu active');
  
  } 
  if(e.target.matches('.jumbotron .lessExperiencie'))
  {
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(lessExperiencie);
    d.querySelector('.lighters').classList.remove('active');    
    seeLessExperience();
    d.querySelector('.lessExperiencie').setAttribute('class','nav-item nav-link lighters menu active');
  
  }
 
});