d.addEventListener('DOMContentLoaded', init);
  d.addEventListener('click',e => {
   
  if(e.target.matches('button') )
  {
     deletePictures();
  }
  if(e.target.matches('.links a'))
  {
    e.preventDefault();
    configureAjaxCalls(e.target.getAttribute('href')); 
    if(e.target.id == 'next')
    {
      contclick++;    
      $numeropagina.innerHTML = "Page: "+contclick; 
    }else
    {
      contclick--;
      $numeropagina.innerHTML = "Page: "+contclick;
    }
    const pagObservable = Rx.Observable
                            .fromEvent(d.querySelector('.links'),'click')
                            .scan(x => x + contclick,1 )
                            .filter(x => x > 0 && x<170)
                            
    const subcsripPag = pagObservable.subscribe();
    subcsripPag.dispose();
  }
  if(e.target.matches('figcaption'))
  {       
    var captions = e.target.querySelector('input');       
    var searchurl = url+"/"+captions.getAttribute('value');
    interruptor = true;
    cancel_subscription = false;
    timer = 2200;
    render(searchurl);
    $('#modal').modal({backdrop: 'static', keyboard: false});
  }
  if(e.target.matches('.jumbo-nav .heaviers') )
  {
    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(heaviers);
    seeHeaviers(); 
    d.querySelector('.lighters').classList.remove('active');  
    d.querySelector('.heaviers').setAttribute('class','nav-item nav-link lighters menu active');
  
  } 
  if(e.target.matches('.jumbo-nav .lighters') )
  {    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(lighters);    
    seeLighters(); 
    d.querySelector('.lighters').setAttribute('class','nav-item nav-link lighters menu active');
    
   } 
  if(e.target.matches('.jumbo-nav .moreExperiencie') )
  {    
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(moreExperiencie); 
    d.querySelector('.lighters').classList.remove('active');    
    seeExperiencie();   
    d.querySelector('.moreExperiencie').setAttribute('class','nav-item nav-link lighters menu active');
  
  } 
  if(e.target.matches('.jumbotron .lessExperiencie'))
  {
    document.querySelector('.jumbotron .navigator').innerHTML = jumbotronComponent(lessExperiencie);
    d.querySelector('.lighters').classList.remove('active');    
    seeLessExperience();
    d.querySelector('.lessExperiencie').setAttribute('class','nav-item nav-link lighters menu active');
  
  }
 
});